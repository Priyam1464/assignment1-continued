package SQLQuery;

import java.io.*;
import java.util.HashMap;

public class Query {
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String query;
    private String path,outPath;
    private HashMap<String,Integer> colsMap;
    public Query(String path,String outPath) throws IOException {
        colsMap=new HashMap<>();
        this.path=path;
        this.outPath=outPath;
        bufferedReader=new BufferedReader(new FileReader(path));
        bufferedWriter=new BufferedWriter(new FileWriter(outPath));
    }
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
    public void readAllColumns() throws IOException {
        String line=null;
        while((line = bufferedReader.readLine()) != null) {
            bufferedWriter.write(line+"\n");
        }
        bufferedWriter.close();
        // Always close files.
        bufferedReader.close();
    }
    public void readSomeColumns(String[] columnsToSelect) throws IOException {
        int counter=0;
            String line=null;
            while((line = bufferedReader.readLine()) != null) {
                String[] lineELemnts=line.split(" ");
                if(counter==0)
                {
                    for(int i=0;i<lineELemnts.length;i++)colsMap.put(lineELemnts[i],i);
                }
                String newLine="";
                for(String cols:columnsToSelect)
                {
                    int index=colsMap.get(cols);
                    newLine+=lineELemnts[index]+" ";
                }
                bufferedWriter.write(newLine+"\n");
                counter++;

            }
            bufferedWriter.close();
            bufferedReader.close();
    }
    public void stringParser() throws IOException {
        String[] splitBySpace=query.split(" ");
        String[] columnsToSelect=splitBySpace[1].split(",");
              if(columnsToSelect[0].equals("*"))
              {
                  readAllColumns();
              }else
              {
                  readSomeColumns(columnsToSelect);
              }

        //System.out.println(columnsToSelect[0]);
    }
}
