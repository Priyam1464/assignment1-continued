package SQLQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class SQLMain {
    public static void main(String[] args) throws IOException {
        Scanner sc=new Scanner(System.in);
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        String inputPath=sc.next();
        String outPath=sc.next();
        String sqlQuery;
        Query query=new Query(inputPath,outPath);
        sqlQuery=bufferedReader.readLine();
        query.setQuery(sqlQuery);
        query.stringParser();
    }
}
