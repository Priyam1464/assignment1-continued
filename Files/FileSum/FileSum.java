package FileSum;

import java.io.*;
import java.util.Scanner;

public class FileSum {

    private String file;
    public FileSum(String file)
    {
        this.file=file;
    }
    public void updateFile()
    {
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
           /* FileReader fileReader =
                    new FileReader(file);*/
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter("C:\\Users\\prichhab\\Documents\\Sapient\\assignment1\\assignment1\\untitled1\\src\\FileSum\\output.txt"));
           // FileWriter fw=new FileWriter("C:\\Users\\prichhab\\Documents\\Sapient\\assignment1\\assignment1\\untitled1\\src\\output.txt");
            while((line = bufferedReader.readLine()) != null) {
                String[] lineELemnts=line.split("\\,");
                int sum=0;
                String newLine="";
                for(String element:lineELemnts)
                {
                    newLine+=element+",";
                    sum+=Integer.parseInt(element);
                }
                bufferedWriter.write(newLine);
                bufferedWriter.write("="+String.valueOf(sum)+"\n");

            }
            bufferedWriter.close();
            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            file + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + file + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }

    }
    public static void main(String[] args) {
        //Scanner s=new Scanner(System.in);
    //    String inputPath=s.next();
      //  String outputPath=s.next();
        FileSum fileSum=new FileSum("C:\\Users\\prichhab\\Documents\\Sapient\\assignment1\\assignment1\\untitled1\\src\\FileSum\\input.txt");
        fileSum.updateFile();
    }
}
