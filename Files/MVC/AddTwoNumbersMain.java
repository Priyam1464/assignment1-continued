package MVC;

public class AddTwoNumbersMain {

    public static void main(String[] args)
    {

        AddTwoNumbersModel addTwoNumbersModel=new AddTwoNumbersModel();
        AddTwoNumbersView addTwoNumbersView=new AddTwoNumbersView();
        addTwoNumbersView.readData(addTwoNumbersModel);
        BusinessLogic businessLogic=new BusinessLogic();
        businessLogic.compute(addTwoNumbersModel);
        addTwoNumbersView.display(addTwoNumbersModel);
        addTwoNumbersModel=null;//dereferecing all objects of garbage collection
        addTwoNumbersView=null;
        businessLogic=null;
        System.gc();//calling garbage collector and finalize.Destructor get called before removal of object
    }
}
